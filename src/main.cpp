#include <iostream>

#include "fibonacci.h"

int main()
{
    int value = 40;

    int res = fibonacci(value);

    std::cout<<value<<"-ое число Фибоначчи равно: "<<res<<std::endl;

    return 0;
}
