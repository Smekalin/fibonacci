#include "fibonacci.h"

int fibonacci(int value)
{
    int current = 0;
    int previous = 1;

    for (int i = 0; i < value; ++i )
    {
        int tmp = current;
        current += previous;
        previous = tmp;
    }
    return current;
}